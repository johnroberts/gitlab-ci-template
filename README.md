Deprecation notice: this project is superceded by https://gitlab.com/johnroberts/vm-gitlab-runner.git

# Summary
Playground for exploring Gitlab CI, main notes [here](docs/gitlab-ci-notes.md)

Includes:
- Example/template `.gitlab-ci.yml` files
- A Gitlab Runner VM (Vagrant/Virtualbox) (at [runner-vm](runner-vm))

## Runner VM Usage
This can be used as a project's CI runner: go to the project's Settings page -> CI/CD -> Runners

On a host with Vagrant + Virtualbox:
```sh
cd runner-vm
vagrant up
vagrant ssh
sudo gitlab-runner register
# Paste https://gitlab.com and the registration token into the correct prompts
```

When prompted, it's useful to add a tag such as `local` to the runner -- this can be used in `.gitlab-ci.yml` to specify where to run jobs.

### Executors
For `docker` executor, you choose a default image. The image `ruby:2.7` suggested by the `gitlab register` command works well.

`shell` executor depends on git (which is pre-installed in the VM during provisioning)