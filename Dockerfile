FROM alpine:latest

RUN apk add --no-cache --update \
        python3 \
        py3-pip

RUN echo "hello from container build"
    
CMD [ "echo", "hello from container run"]

