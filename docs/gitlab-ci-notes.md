# Steps
Make/have a Gitlab repo (and owner/maintainer privileges)

Create a `.gitlab-ci.yml` at project root

Add jobs in stages to build, test, package, and deploy the project

# Concepts
*Job*: unit of execution within a pipeline. Each job has a `script:` section, and belongs to a stage

*Stage*: a grouping of jobs that execute at a given place within a pipeline

*Runners*: hosts that execute jobs in the pipeline. gitlab.com hosts shared free-tier runners; self-hosting is also possible.

*Executors*: pre-configured types of runners for various scenarios. Common ones are `shell` and `docker`

*Artifacts*: files/directories created by a job; useful for packaging, compilation, rendering templates to files, etc.

*Environment*: GitLab's representation of a deployment environment (e.g. dev, staging, production). There is a set of environment variables per environment.

*`.gitlab-ci.yml`*: main configuration for pipeline

*Pipeline*: the end-to-end execution unit that executes jobs stage-by-stage.

# Misc
- Various Gitlab CI functionality is available for free-tier, or premium-only. I'll stick with free-tier stuff by default.

- `$CI_COMMIT_TAG` is present when you push a tagged commit; this can be used to selectively trigger pipeline jobs (like a build or a test) using `rules:if`

# Resources
- Quick start: https://docs.gitlab.com/ee/ci/quick_start/
- Gitlab CI reference: https://docs.gitlab.com/ee/ci/yaml/index.html